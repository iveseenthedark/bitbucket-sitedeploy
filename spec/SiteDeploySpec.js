describe('SiteDeploy', () => {
	const site_deploy = require('../index');

	it('should fail if missing required parameters', async () => {
		await expectAsync(site_deploy({ username: 'Alex', version: '1.0' })).toBeRejectedWithError('source not defined');
		await expectAsync(site_deploy({ username: 'Alex', source: './dist' })).toBeRejectedWithError('version not defined');
		await expectAsync(site_deploy({ version: '1.0', source: './dist' })).toBeRejectedWithError('username not defined');
	});
});
