const git = require('simple-git/promise');
const vfs = require('vinyl-fs');
const del = require('del');
const os = require('os');

const copy = async (src, dst) =>
	new Promise((resolve, reject) => {
		vfs
			.src(src)
			.pipe(vfs.dest(dst))
			.on('finish', resolve)
			.on('error', reject);
	});

const check_required = opts => {
	Object.entries(opts).forEach(([key, value]) => {
		if (typeof value === 'undefined') {
			throw new Error(`${key} not defined`);
		}
	});
};

const build_git_url = ({ ssh, host, username }) => {
	return ssh ? `git@${host}:${username}` : `https://${username}@${host}/${username}`;
};

/**
 * @param {*} opts
 * {
 *    "username"     : "Git repo username (required)"
 *    "version"      : "New site version (required)"
 *    "source"       : "Path of the site source (required)"
 *    "context"      : "Site context or empty for root context"
 *    "delete_paths" : "Paths to remove from version control. If empty all non-git files will be removed, as well as any CNAME files or .md files. Paths are relative to the root of the project"
 *    "host"         : "The Git host to use (e.g bitbucket.org or github.com). Default is bitbucket.org"
 *    "ssh"          : "Whether to use SSH or HTTPS to perform git requests. Default is false"
 *    "tmp"			 : "Location to store temporary files"
 * }
 */
module.exports = async ({
	username,
	version,
	source,
	context,
	delete_paths,
	tmp,
	host = 'bitbucket.org',
	ssh = false,
}) => {
	check_required({ username, version, source });

	const domain = host.split('.')[0];
	const site = `${username}.${domain}.io`;
	const git_host = build_git_url({ username, host, ssh });

	const temp_folder = tmp || `${os.tmpdir()}/${site}_${Date.now()}`;
	const git_source = `${temp_folder}` + (context ? `/${context}` : ``);

	// Clone project and create new version branch
	await git().clone(`${git_host}/${site}.git`, temp_folder);
	await git(temp_folder).checkoutLocalBranch(version);

	// Delete current files and replace with new
	delete_paths = delete_paths ? delete_paths.map(path => `${git_source}${path}`) : [`${git_source}/**`];
	await del([...delete_paths, `!${git_source}`, `!${git_source}/.git`, `!${git_source}/*.md`, `!${git_source}/CNAME`], {
		force: true,
	});
	await copy(`${source}/**`, `${git_source}`);

	// Add updates to commit
	await git(temp_folder).raw(['add', '--all']);
	await git(temp_folder).commit(`[Site Update]: ${context || 'ROOT'} - v${version}`);

	// Push
	await git(temp_folder).push('origin', version);

	// Remove tmp folder
	await del([temp_folder], { force: true });
};
