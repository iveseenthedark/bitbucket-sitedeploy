# Static Site Deploy

A simple (poorly named) script for deploying static sites to your Git repository (i.e. Github Pages or Bitbucket).

Note this neither needs to work with Bitbucket or Gulp. It just started out that way.

## Example Usage

This will deploy the code in the `./dist` folder to the iveseenthedark site project on Bitbucket.

```js
gulp.task('site-deploy', cb => {
	sitedeploy({
		username: 'iveseenthedark',
		version: require('./package.json').version,
		source: `./dist`,
	})
		.then(cb())
		.catch(err => cb(err));
});
```

## API

### sitedeploy(options)

Checks out the static site project, copies the content of the `source` folder into the directory, creates a version branch and pushes back to the repo.

#### Options

##### options.username

The git repo username (required)

##### options.version

New site version (required)

##### options.source

Path of the site source (required)

##### options.context

If supplied this will create a folder under the root folder and copy the site content into there. If not used, the root context is used.

##### options.delete_paths

Paths to remove from version control. If empty all non-git files will be removed. Paths are relative to the root of the project

##### options.host

The Git host to use (e.g bitbucket.org or github.com). Default is bitbucket.org

##### options.ssh

Whether to use SSH or HTTPS to perform git requests. Default is false

##### options.tmp

Location to store temporary files. Defaults to operating system default path
